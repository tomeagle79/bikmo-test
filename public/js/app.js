class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      language: "",
      greeting: "",
    };
  }

  handleChange = (e) => {
    const { value, name } = e.target;
    // console.log(name, value);
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = () => {
    const { userName, language } = this.state;
    fetch(`/api/you/introduce?name=${userName}&language=${language}`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        if (data.status === "error") {
          this.setState({ greeting: data.message });
          return;
        }
        this.setState({ greeting: data.data.greeting });
      });
  };

  handleReset = () => {
    this.setState({
      greeting: "",
      userName: "",
      language: "",
    });
  };

  render() {
    const { userName, language, greeting } = this.state;
    const disabled = !(language !== "" && userName.trim() !== "");

    if (greeting !== "") {
      return (
        <section className="py-3">
          <p>{greeting}</p>
          <button className="btn btn-primary mt-3" onClick={this.handleReset}>
            Reset
          </button>
        </section>
      );
    }

    return (
      <section className="py-3">
        <p>Please introduce yourself.</p>

        <div className="form-group">
          <label htmlFor="name">Name:</label>

          <input
            type="text"
            className="form-control"
            onChange={(e) => this.handleChange(e)}
            name="userName"
            value={userName}
          />
        </div>
        <div className="form-group">
          <label htmlFor="language">Language:</label>

          <select
            name="language"
            className="form-control"
            value={language}
            onChange={(e) => this.handleChange(e)}
          >
            <option disabled value="">
              Please choose a language
            </option>
            <option value="english">English</option>
            <option value="deutsch">Deutsch</option>
            <option value="backwards">Backwards</option>
          </select>
        </div>
        <p>
          <button
            disabled={disabled}
            className="btn btn-primary"
            onClick={this.handleSubmit}
          >
            Submit
          </button>
        </p>
      </section>
    );
  }
}

const domContainer = document.querySelector("#root");
ReactDOM.render(<App />, domContainer);
